package com.wackowacko.sfgpre2.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class E404Exception extends RuntimeException {
  private static final long serialVersionUID = 10002L;

  public E404Exception() {
    super();
  }

  public E404Exception(String msg) {
    super(msg);
  }
  // ...
}
