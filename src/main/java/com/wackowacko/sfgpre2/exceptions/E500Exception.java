package com.wackowacko.sfgpre2.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class E500Exception extends RuntimeException {
  private static final long serialVersionUID = 10003L;

  public E500Exception() {
    super();
  }

  public E500Exception(String msg) {
    super(msg);
  }

  // ...
}
