package com.wackowacko.sfgpre2.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class E400Exception extends RuntimeException {
  private static final long serialVersionUID = 10001L;

  public E400Exception() {
    super();
  }

  public E400Exception(String msg) {
    super(msg);
  }
  // ...
}
