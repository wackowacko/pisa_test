package com.wackowacko.sfgpre2.models;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.annotation.Id;


@Document(collection="Product")
public class Product {
    @Id
    public String _id;
    public String art;
    public String title;


    /*
    */
    public Product() {
      // ...
    }


    public Product(String title) {
      this.title = title;
    }


    @Override
    public String toString() {
        return String.format(
          "Product[id=%s, art='%s', title='%s']", _id, art, title);
    }

}
