package com.wackowacko.sfgpre2.models;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;


public interface ProductRepo extends MongoRepository<Product, String> {
    public Product findByArt(String art);
    public List<Product> findByTitle(String title);

    @Query("{'active': true, '_id': ?0}")
    public Product getById(String id);

    @Query("{'active': true}")
    public List<Product> getAllActive();

    @Query("{'active': true}")
    public List<Product> getActiveSorted(Sort sort);

    @Query("{'props.p_price': {$gt: 50000}}")
    public List<Product> getSuperPrice();

    @Query("{'active': true, 'title': {$regex: ?0, $options: 'i'}}")
    public List<Product> searchProducts(String q);
}
