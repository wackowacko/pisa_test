package com.wackowacko.sfgpre2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sfgpre2Application {

	public static void main(String[] args) {
		// System.setProperty("spring.devtools.restart.enabled", "true");
		SpringApplication.run(Sfgpre2Application.class, args);
	}

}
