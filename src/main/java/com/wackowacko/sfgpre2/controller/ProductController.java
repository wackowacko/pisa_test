
package com.wackowacko.sfgpre2.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import com.wackowacko.sfgpre2.models.Product;
import com.wackowacko.sfgpre2.models.ProductRepo;
import com.wackowacko.sfgpre2.exceptions.E404Exception;
import com.wackowacko.sfgpre2.exceptions.E400Exception;
import com.wackowacko.sfgpre2.exceptions.E500Exception;


import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import com.wackowacko.sfgpre2.exceptions.E404Exception;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.validation.Valid;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;


@RestController
@RequestMapping("product")
public class ProductController {
  @Autowired
	private ProductRepo repo;

  private int postRequestCounter = 0;


  /* Get all
  */
  @GetMapping
  public List<Product> getAll() {
    return repo.getAllActive();
  }


  /* Get all sorted
  */
  @GetMapping("sorted")
  public List<Product> getAllSorted() {
    return repo.getActiveSorted(new Sort(Sort.Direction.DESC, "createdAt"));
  }


  /* Get by Mongo ObjectId
  */
  @GetMapping("{id}")
  public Product getOne(@PathVariable String id) {
    Product result = repo.getById(id);
    if (result == null) throw new E404Exception();
    return result;
  }


  /* Create new
  */
  @PostMapping
  public Product addProduct(@Valid @RequestBody Product p) {
    postRequestCounter++; // HZ

    // Simple checks
    if (p.title == null || p.art == null ||
        p.title.length() < 1 || p.art.length() < 1) {
        throw new ResponseStatusException(
          HttpStatus.NOT_FOUND, "'title' and 'art' is required");
    }

    // Save
    try { repo.save(p); }
    catch(Exception e) { throw new E400Exception(); } // Todo

    return p;
  }


  /* Delete
  */
  @DeleteMapping("{id}")
  public String delete(@PathVariable String id) {
    String result = "{result: 1}"; // Todo
    try { repo.deleteById(id); }
    catch(Exception e) { throw new E500Exception(); } // Todo
    return result;
  }


  /* Update
  */
  @PutMapping("{id}")
  public Product setTitle(@PathVariable String id,
                          @Valid @RequestBody Product data) {

    Product product = repo.getById(id);
    if (product == null) throw new E404Exception();

    if (data.title != null && data.title.length() > 1) product.title = data.title; // todo more good checks
    if (data.art != null && data.art.length() > 1) product.art = data.art; // todo more good checks

    repo.save(product); // Todo. Check exceptions, etc
    return product;
  }
}
