


package com.wackowacko.sfgpre2.controller;


import java.util.stream.Collectors;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.wackowacko.sfgpre2.exceptions.E500Exception;
import java.util.concurrent.CompletableFuture;
import org.springframework.web.client.RestTemplate;
import com.wackowacko.sfgpre2.models.Product;
import com.wackowacko.sfgpre2.models.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;


@RestController
@RequestMapping("outside")
public class OutsideController {
  private static final Logger logger = LoggerFactory.getLogger(FilterController.class);
  final String extServiceUri = "https://jsonplaceholder.typicode.com/todos/";


  /*
  */
  @GetMapping()
  public String getIndex() {
      RestTemplate restTemplate = new RestTemplate();
      String result = restTemplate.getForObject(extServiceUri, String.class);
      return result;
  }


  /*
  */
  @GetMapping("async1")
  public String getAsync1() throws InterruptedException, ExecutionException  {
      RestTemplate restTemplate = new RestTemplate();
      //           result = restTemplate.getForObject(extServiceUri, String.class);

      CompletableFuture<String> task1 = CompletableFuture.supplyAsync(() -> {
        /* test */ try { Thread.sleep(5000); } catch(Exception e) {}
        String res = restTemplate.getForObject(extServiceUri, String.class);
        return res;
      }).handle((obj, err) -> {
        if (err != null) logger.error(err.getMessage());
        if (obj != null) return obj;
        return "";
      });

      CompletableFuture<String> task2 = CompletableFuture.supplyAsync(() -> {
        /* test */ try { Thread.sleep(5000); } catch(Exception e) {}
        String res = restTemplate.getForObject(extServiceUri, String.class);
        return res;
      }).handle((obj, err) -> {
        if (err != null) logger.error(err.getMessage());
        if (obj != null) return obj;
        return "";
      });


      // Result
      CompletableFuture<String> result = task1.thenCombine(task2, (a, b) -> a + b);
      return result.get();
  }


  /*
  */
  private CompletableFuture<String> simpleFetchData(String uri) {
      RestTemplate restTemplate = new RestTemplate();
      return CompletableFuture.supplyAsync( () -> {
        /* !!! Nb !!! TEST DELAY */ try { Thread.sleep(1500); } catch(Exception e) {}
        String res = restTemplate.getForObject(uri, String.class);
        return res;
      }).handle((obj, err) -> {
        if (err != null) logger.error(err.getMessage());
        if (obj != null) return obj;
        return "";
      });
  }

  /*
  */
  @GetMapping("async2")
  public String getAsync2() throws InterruptedException, ExecutionException  {
      List<String> urls = Arrays.asList(
        "https://jsonplaceholder.typicode.com/posts",
        "https://jsonplaceholder.typicode.com/comments",
        "https://jsonplaceholder.typicode.com/todos",
        "https://jsonplaceholder.typicode.com/photos"
      );

      // Create tasks (CompletableFuture) array
      List<CompletableFuture<String>> tasks = urls.stream()
        .map(uri -> simpleFetchData(uri))
        .collect(Collectors.toList());

      // Convert to List<String>...
      CompletableFuture<List<String>> all_fts = CompletableFuture
          .allOf( // to CompletableFuture[]
            tasks.toArray(new CompletableFuture[tasks.size()]))
          .thenApply(f -> { return tasks.stream()
            .map(el -> el.join()).collect(Collectors.toList());
          });


      // Result
      String result = all_fts.get().toString();
      return result;
  }


  /*
  */
  @GetMapping("{task_id}")
  public String getById(@PathVariable String task_id) {
      RestTemplate restTemplate = new RestTemplate();
      String result = restTemplate.getForObject(extServiceUri+task_id, String.class);
      return result;
  }
}
