
package com.wackowacko.sfgpre2.controller;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.stream.Collectors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import com.wackowacko.sfgpre2.models.Product;
import com.wackowacko.sfgpre2.models.ProductRepo;
import com.wackowacko.sfgpre2.exceptions.E404Exception;
import com.wackowacko.sfgpre2.exceptions.E400Exception;
import com.wackowacko.sfgpre2.exceptions.E500Exception;


import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.validation.Valid;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;


@RestController
@RequestMapping("filter")
public class FilterController {
  private static final Logger logger = LoggerFactory.getLogger(FilterController.class);


  @Autowired
	private ProductRepo repo;


  /* Test data
  */
  // public List<Map<String, String>> products = new ArrayList<Map<String, String>>() {{
  //   add(new HashMap<String, String>() {{ put("_id", "001"); put("title", "product 1"); }});
  //   add(new HashMap<String, String>() {{ put("_id", "002"); put("title", "product 2"); }});
  // }};


  /* Find. Filter way
  */
  @GetMapping("dummy")
  public List<Product> dummySearch(@RequestParam String q) {
    if (q == null || q.length() < 2) throw new ResponseStatusException(
           HttpStatus.NOT_FOUND, "Search sring too short");

    List<Product> data = repo.getAllActive();
    if (data == null || data.size() < 1) throw new E404Exception();

    return data.stream()
      .filter(el -> el.title.contains(q) || el.art.contains(q)) // filter by part of 'title' or 'art'
      .collect(Collectors.toList());
  }


  /* Find. Mongo way
  */
  @GetMapping("bello")
  public List<Product> belloSearch(@RequestParam String q) {
    if (q == null || q.length() < 2) throw new ResponseStatusException(
           HttpStatus.NOT_FOUND, "Search sring too short");

    List<Product> result = repo.searchProducts(q);
    return result;
  }
}
